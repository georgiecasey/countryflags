![](https://www.georgiecasey.com/misc/country_flags_screenshot_1.png){width=500 height=100}

# Assumptions
 - I'm using Glide to load images from the web and letting that handle caching, so I'm not saving flag images to disk or to the DB or anything. 
 - Also relying on a Glide failure to load listener to show an error dialog to the user instead of checking if a flag preview exists. So you'll get the same error if there's no internet for example.
 - This isn't ideal as Glide observes the search string LiveData and will attempt to load on a user screen rotation. So you'd get the same error message if user rotates. I initially catered for this by using a SingleLiveEvent but Google changed their recommended best practices multiple times and now tell you to just hold state in the ViewModel if error is shown, so that's what I've done.
 - That Github page of flags has an accompaning JSON for the country names so I'm reading that instead of scraping and parsing the HTML page. I GET this everytime a user saves a flag to the DB as country name is needed when searching the list. 
 - No tests of any kind because of time restraints.
 - If a user searches and gets a flag preview, then enters a new search string, clicks the button to search for that flag, but clicks the save button quick enough, the new search string will be saved to the DB. 