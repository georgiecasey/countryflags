package com.georgiecasey.countryflags.di

import android.content.Context
import androidx.room.Room
import com.georgiecasey.countryflags.BuildConfig
import com.georgiecasey.countryflags.data.CountryFlagDatabase
import com.georgiecasey.countryflags.data.Repository
import com.georgiecasey.countryflags.data.model.CountryFlagDao
import com.georgiecasey.countryflags.data.retrofit.CountryNamesRetrofit
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {
  @Provides
  @Singleton
  fun provideOkHttp(): OkHttpClient.Builder =
    OkHttpClient.Builder()
      .dispatcher(Dispatcher().apply {
        maxRequestsPerHost = 1
      })

  @Provides
  @Singleton
  fun provideMoshi(): Moshi =
    Moshi.Builder()
      .build()

  @Provides
  @Singleton
  fun provideRetrofit(): CountryNamesRetrofit =
    Retrofit.Builder()
      .addConverterFactory(MoshiConverterFactory.create(provideMoshi()))
      .baseUrl("https://raw.githubusercontent.com/")
      .client(provideOkHttp()
        .build())
      .build()
      .create(CountryNamesRetrofit::class.java)

  @Provides
  @Singleton
  fun provideDatabase(@ApplicationContext context: Context): CountryFlagDatabase =
    Room.databaseBuilder(context.applicationContext, CountryFlagDatabase::class.java, "db")
      .apply {
        if (BuildConfig.DEBUG) fallbackToDestructiveMigration()
      }
      .build()

  @Provides
  @Singleton
  fun provideCountryFlagDao(database: CountryFlagDatabase): CountryFlagDao =
    database.countryFlagDao()

  @Provides
  @Singleton
  fun provideRepository(
    countryFlagDatabase: CountryFlagDatabase,
    countryNamesGitHub: CountryNamesRetrofit
  ): Repository {
    return Repository(countryFlagDatabase, countryNamesGitHub)
  }

}
