package com.georgiecasey.countryflags.data

import com.georgiecasey.countryflags.data.model.CountryFlag
import com.georgiecasey.countryflags.data.retrofit.CountryNamesRetrofit

class Repository(
    val database: CountryFlagDatabase,
    val countryNamesGitHub: CountryNamesRetrofit
) {

    suspend fun getSavedCountries(query: String? = null): List<CountryFlag> {
        if (query.isNullOrBlank()) {
            return database.countryFlagDao().fetchAll()
        } else {
            return database.countryFlagDao().searchCountryCodesAndNames(query)
        }
    }

    suspend fun saveCountryCode(countryCode: String) {
        val countryNames = countryNamesGitHub.getCountryNames()
        val countryNamesMap = countryNames.body()
        val countryName = countryNamesMap?.getOrDefault(countryCode, null) ?: null
        database.countryFlagDao().insert(CountryFlag(countryCode, countryName))
    }
}