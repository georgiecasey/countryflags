package com.georgiecasey.countryflags.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.georgiecasey.countryflags.data.model.CountryFlag
import com.georgiecasey.countryflags.data.model.CountryFlagDao

@Database(
    entities = [
        CountryFlag::class
    ],
    version = 1
)
abstract class CountryFlagDatabase : RoomDatabase() {
    abstract fun countryFlagDao(): CountryFlagDao
}
