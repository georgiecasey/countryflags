package com.georgiecasey.countryflags.data.model

import androidx.room.*

@Entity(tableName = "country_flag")
data class CountryFlag(
    @PrimaryKey
    @ColumnInfo(name = "code")
    val countryCode: String,
    @ColumnInfo(name = "name")
    val countryName: String?
)

@Dao
interface CountryFlagDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(data: CountryFlag?): Long

    @Query("SELECT * FROM country_flag WHERE code = :countryCode LIMIT 1")
    suspend fun fetchByCountryCode(countryCode: String): CountryFlag?

    @Query("SELECT * FROM country_flag")
    suspend fun fetchAll(): List<CountryFlag>

    @Query("SELECT * FROM country_flag WHERE code LIKE '%' || :query || '%' OR name LIKE '%' || :query || '%'")
    suspend fun searchCountryCodesAndNames(query: String): List<CountryFlag>
}