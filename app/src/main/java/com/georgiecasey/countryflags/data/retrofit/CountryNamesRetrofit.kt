package com.georgiecasey.countryflags.data.retrofit

import retrofit2.http.GET
import retrofit2.Response

interface CountryNamesRetrofit {
    @GET("https://raw.githubusercontent.com/hampusborgos/country-flags/main/countries.json")
    suspend fun getCountryNames(): Response<Map<String, String>>
}