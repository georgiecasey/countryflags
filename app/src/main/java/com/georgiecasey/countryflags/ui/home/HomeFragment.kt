package com.georgiecasey.countryflags.ui.home

import android.graphics.drawable.Drawable
import androidx.lifecycle.Observer
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import android.view.ViewGroup
import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.georgiecasey.countryflags.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment: Fragment() {

    val homeViewModel: HomeViewModel by viewModels()
    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val searchString = binding.search
        val searchRemote = binding.searchRemote
        val flag = binding.flag
        val save = binding.save
        val loading = binding.loading

        homeViewModel.flagImageUrl.observe(viewLifecycleOwner, Observer {
            Glide
                .with(this)
                .load(it)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        if (homeViewModel.shouldWeShowGlideError()) {
                            val errorFragment = ErrorDialogFragment()
                            errorFragment.show(parentFragmentManager, "country_error")
                        }
                        save.isEnabled = false
                        loading.visibility = View.INVISIBLE
                        return true
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        save.isEnabled = true
                        loading.visibility = View.INVISIBLE
                        return false
                    }
                })
                .into(flag)
        })


        searchRemote.setOnClickListener {
            loading.visibility = View.VISIBLE
            homeViewModel.searchForFlagPreviewRemote(searchString.text.toString().uppercase())
        }

        save.setOnClickListener {
            homeViewModel.saveCountryCode()
        }
    }
}
