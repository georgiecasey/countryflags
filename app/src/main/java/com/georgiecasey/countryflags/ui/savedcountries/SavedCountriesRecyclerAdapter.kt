package com.georgiecasey.countryflags.ui.savedcountries

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.georgiecasey.countryflags.data.model.CountryFlag
import com.georgiecasey.countryflags.databinding.ItemCountryFlagBinding

class SavedCountriesRecyclerAdapter :
    RecyclerView.Adapter<SavedCountriesRecyclerAdapter.ViewHolder>() {

    private var items = mutableListOf<CountryFlag>()

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding = ItemCountryFlagBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items.get(position))
    }

    fun setItems(newItems: List<CountryFlag>) {
        this.items.clear()
        this.items.addAll(newItems)
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: ItemCountryFlagBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(flag: CountryFlag) {
            binding.tvCountryCode.text = flag.countryCode
            Glide
                .with(binding.root.context)
                .load("https://raw.githubusercontent.com/hampusborgos/country-flags/main/png100px/"+flag.countryCode.lowercase()+".png")
                .into(binding.imgFlag)
        }
    }
}
