package com.georgiecasey.countryflags.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.georgiecasey.countryflags.data.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel
@Inject constructor(
    private val repository: Repository
) : ViewModel() {
    private val _searchString = MutableLiveData<String>()
    val searchString: LiveData<String> = _searchString

    private val _flagImageUrl = MutableLiveData<String>()
    val flagImageUrl: LiveData<String> = _flagImageUrl

    var glideErrorShown = false

    fun searchForFlagPreviewRemote(query: String) {
        glideErrorShown = false
        _searchString.value = query
        _flagImageUrl.value = "https://raw.githubusercontent.com/hampusborgos/country-flags/main/png100px/"+query.lowercase()+".png"
    }

    fun saveCountryCode() {
        viewModelScope.launch {
            searchString.value?.let {
                repository.saveCountryCode(it)
            }
        }
    }

    fun shouldWeShowGlideError(): Boolean {
        if (glideErrorShown == false) {
            glideErrorShown = true
            return true
        }
        return false
    }
}