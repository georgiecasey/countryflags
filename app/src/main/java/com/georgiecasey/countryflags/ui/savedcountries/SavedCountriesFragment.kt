package com.georgiecasey.countryflags.ui.savedcountries

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.georgiecasey.countryflags.databinding.FragmentSavedCountriesBinding
import com.georgiecasey.countryflags.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SavedCountriesFragment: Fragment(), SearchView.OnQueryTextListener {

    private lateinit var binding: FragmentSavedCountriesBinding
    val savedCountriesViewModel: SavedCountriesViewModel by viewModels()
    private val savedCountriesRecyclerAdapter by lazy { SavedCountriesRecyclerAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        binding = FragmentSavedCountriesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rvSavedCountries = binding.savedCountriesList
        rvSavedCountries.adapter = savedCountriesRecyclerAdapter
        rvSavedCountries.layoutManager = LinearLayoutManager(requireContext())

        savedCountriesViewModel.savedCountriesLiveData.observe(viewLifecycleOwner, Observer { countries ->
            savedCountriesRecyclerAdapter.setItems(countries)
        })
        savedCountriesViewModel.getSavedCountries()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_savedcountries, menu)
        val searchItem = menu.findItem(R.id.search_country)
        val searchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(this)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        savedCountriesViewModel.getSavedCountries(query)
        return true
    }

    override fun onQueryTextChange(query: String?): Boolean {
        savedCountriesViewModel.getSavedCountries(query)
        return true
    }
}
