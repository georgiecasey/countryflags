package com.georgiecasey.countryflags.ui.savedcountries

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.georgiecasey.countryflags.data.Repository

import com.georgiecasey.countryflags.data.model.CountryFlag
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SavedCountriesViewModel
@Inject constructor(
    private val repository: Repository
) : ViewModel() {

    private val _savedCountriesLiveData = MutableLiveData<List<CountryFlag>>()
    val savedCountriesLiveData: LiveData<List<CountryFlag>> = _savedCountriesLiveData

    fun getSavedCountries(query: String? = null) {
        viewModelScope.launch {
            _savedCountriesLiveData.postValue(repository.getSavedCountries(query))
        }
    }


}